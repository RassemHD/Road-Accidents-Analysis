import logging
from pathlib import Path
import pydoop.hdfs as fs

logging.basicConfig(level=logging.INFO)

def create_views(spark,input_dir):

	files = fs.hdfs().list_directory(input_dir)

	for f in files:

		spark.read.options(
			header = "true", 
			inferSchema = "true", # automatically guess the data types for each field
			nullValue = "NA", # string representation of null value
			quote = "\"", # character used to enclose the string values. critical some fields contains commas. default value is the double quote
			mode = "PERMISSIVE" # defines the method for dealing with a corrupt record: PERMISSIVE, DROPMALFORMED, FAILFAST
		).load(
			format="csv",
			path=f['name']
		).createOrReplaceTempView(
			Path(f['name']).stem
		)

	logging.info("views successfully created!")


def save_data(sql_df,output_dir):

	sql_df.write.options(
		header = "true", 
		inferSchema = "true",
		nullValue = "NA"
	).save(
		format="csv",
		mode="overwrite",
		path=output_dir
	)

	logging.info("data frame successfully saved!")