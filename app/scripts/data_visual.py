import matplotlib.pyplot as plt

#fonction générant un diagramme de barres
#prend 3 paramètres
# df : data frames contenant deux colonnes
# label : noms des axe x puis y
# titre : nom de la figure
# color : couleure du diagramme (soit rgba , soit str)
def bar_diag(df,label,titre,color):
    
    df.collect()
    data = df.rdd.map(lambda row: [row[0],row[1]]).collect()
    plt.xticks(rotation = -90)
    plt.xlabel(label[0])
    plt.ylabel(label[1])
    plt.title(titre)
    x = list(zip(*data))[0]
    y = list(zip(*data))[1]
    plt.bar(x=x,height=y,color = color)
    
    return ;

#fonction générant un diagramme de type camembert
#prend 2 paramètres
# df : dataframe contenant deux colonnes 
#       la première contien les labels
#       la seconde contient les valeurs 
# titre : titre de la figure
def camembert_diag(df,titre):
    
    df.collect()
    data = df.rdd.map(lambda row: [row[0],row[1]]).collect()
    plt.title(titre)
    plt.pie(x = list(zip(*data))[1], labels = list(zip(*data))[0],startangle=90)
    
    return;

#fonction générant une courbe
#prend 3 paramètres
# df : data frames contenant deux colonnes
# label : noms des axe x puis y
# titre : nom de la figure
# color : couleure du diagramme (soit rgba , soit str)

def plot_diag(df,label,titre,color):
    
    df.collect()
    data = df.rdd.map(lambda row: [row[0],row[1]]).collect()
    
    plt.xlabel(label[0])
    plt.ylabel(label[1])
    plt.xticks(rotation = -90)
    plt.title(titre)
    x = list(zip(*data))[0]
    y = list(zip(*data))[1]
    plt.plot(x,y,color = color)
    
    return ;

#fonction générant une box plot
#prend 2 paramètres
# df : dataframe contenant une seule colonne
#  par defaut on prend la colonne 0
# titre : titre de la figure
def box_plot_diag(df,titre):
    
    df.collect()
    data = df.rdd.map(lambda row: row[0]).collect()
    plt.title(titre)
    plt.boxplot(data)
    
    return;
