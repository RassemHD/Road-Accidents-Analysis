#!/bin/bash

IMAGE_NAME=hadoop-cluster:latest
NB_SLAVES=2

if [[ "$1" == "-setup" ]]; then

	# build the image
	sudo docker build -t $IMAGE_NAME .

	# create a network for the containers
	sudo docker network create --driver=bridge hadoop-network

	# start hadoop master container
	echo -e "\nstart container hadoop-master ..."
	sudo docker run -itd --net=hadoop-network -p 50070:50070 -p 8088:8088 -p 7077:7077 -p 16010:16010 -p 8080:8080 -p 10000:10000 \
	     --name hadoop-master \
	     --mount type=bind,source="$(pwd)"/app,target=/root/app \
	     --hostname hadoop-master $IMAGE_NAME
	     
	# start hadoop slave containers
	i=1
	while [ $i -le $NB_SLAVES ]
	do
		echo -e "start container hadoop-slave$i ..."
		port=$(( 8040 + $i ))
		sudo docker run -itd --net=hadoop-network -p $port:8042 \
			 --name hadoop-slave$i --hostname hadoop-slave$i $IMAGE_NAME
		((i++))
	done

	# get into hadoop master container and start services
	sudo docker exec -it hadoop-master /root/sbin/start-hdfs.sh
	sudo docker exec -it hadoop-master bash

elif [[ "$1" == "-start" ]]; then

	sudo docker start hadoop-master
	
	i=1
	while [ $i -le $NB_SLAVES ]
	do
		sudo docker start hadoop-slave$i
		((i++))
	done

	# get into hadoop master container and start services
	sudo docker exec -it hadoop-master /root/sbin/start-hdfs.sh
	sudo docker exec -it hadoop-master bash

elif [[ "$1" == "-stop" ]]; then

	# stop all active containers
	sudo docker stop $(
		sudo docker ps -q --filter ancestor=$IMAGE_NAME
	)

elif [[ "$1" == "-kill" ]]; then

	# remove all containers
	sudo docker rm $(
		sudo docker stop $(
			sudo docker ps -a -q --filter ancestor=$IMAGE_NAME
		)
	)

	# remove network
	sudo docker network rm hadoop-network

else

	echo "Usage: ./cluster.sh -OPTION
	-setup: build image and launch all containers
	-start: launch the cluster (already created)
	-stop: stop all active containers
	-kill: remove all containers"
fi
