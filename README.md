# Projet Big Data

# Étude des accidents routiers en France

## Data set 

https://www.kaggle.com/ahmedlahlou/accidents-in-france-from-2005-to-2016/data

NB: ajout d'un fichier contenant des données géographiques utiles, qui ne sont pas disponibles sur Kaggle: noms des départements, noms des régions ...etc 

## Usage

#### 1)- Construcion de cluster 
        ./cluster.sh
   
#### 2)- Copie des données sur HDFS (master container: bash)
        hdfs dfs -mkdir -p app
        hdfs dfs -put app/input-data app

#### 3)- Lancement de jupyter note book (master container: bash)
        ./sbin/start-jupyter.sh

## Éléments à étudier 

### Temps

- jours et périodes touchées
- heurs de pointe / heures creuses
- jours firiés ou pas
- lumière du jour

### Utilisateurs

- personne impliquée dans l'acident (driver, pieton..)
- sex & age
- raison de sortie (work, school..)
- Sécurité (ceintures, casques..)

### Localisation

département number: dep
commune number: com
région
Localisation: 1 Out of agglomeration, 2 - In built-up areas

### Conditions climatiques

atmospheric conditions: (caracteristics: atm) 1-Normal, 2-Light rain ...
surface condition (places: surf)

### Rues

- num rues

### Places

- infrastructures (ponts, tunnels) 
- forme de la rue (ligne doite, S, virage) situ
- catégories de la rue catr

### Véhicules

- catégorie

### Caractiristics

- Type de collision (front,arrière) col
- type d'intersection int
